from click import group, option, command
from .create_config import get_project_config
from importlib import __import__

project_config = get_project_config()

@group()
def cli():
    pass

try:
    location, last_module = project_config['cli']['module'].split(':')
    module = __import__(location, globals(), locals(), [last_module], 0)
    if hasattr(module, 'cli'):
        cli = module.cli
except ImportError:
    pass

@command('run-server')
@option('--debug', '-d', is_flag=True, default=False, help='Run the server in debug mode')
@option('--prod', is_flag=True, default=False, help='Run the server in production mode')
@option('--port', '-p', default=8080, help='Run the server on the specified port')
@option('--host', '-p', default="127.0.0.1", help='Run the server on the specified interface')
def run_server(debug=False, prod=False, port=8080, host="127.0.0.1"):
    if prod:
        import subprocess
        subprocess.run(["gunicorn", "-b", f"{host}:{port}", "{}.wsgi:server".format(
            project_config['project']['package']
        )])
        return

    wsgi = __import__("{}.wsgi".format(
        project_config['project']['package']
    ), globals(), locals(), ['server'], 0)

    wsgi.server.run(host=host, port=port, debug=debug)
        

    
cli.add_command(run_server)