import tempfile, subprocess
from os.path import isdir, join, exists
from create_config import get_project_config
from contextlib import redirect_stdout, redirect_stderr
import io
from shlex import quote

def main():
  sourcefile = tempfile.NamedTemporaryFile(mode='w', delete=False)
  project_config = get_project_config()
  name = project_config['project'].get('name')
  project_env = './env'
  activate = join(project_env, 'bin/activate')
  if isdir(project_env) and exists(activate):
    sourcefile.write('echo "Virtualenv already exists. Continuing...\\n"\n')
  else:
    results = subprocess.run(['virtualenv', '-p', 'python3', project_env], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sourcefile.write('echo "Results of command:\\n{}\\n{}"\n'.format(results.stdout.decode('unicode_escape'), results.stderr.decode('unicode_escape')))
    sourcefile.write('echo "Virtualenv was created.\\n"\n')
  
  sourcefile.write('echo -e "\\nRun \'deactivate\' when finished\\n"\n')
  sourcefile.write('source '+activate+'\n')
  sourcefile.close()
  print(sourcefile.name)

if __name__ == "__main__":
  main()