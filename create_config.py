#!/usr/bin/python3
import os, sys

CONFIG_TEMPLATE = '/config.template'
CONFIG_DEST = '/config.py'

def get_project_config():
    import importlib
    try:
        yaml = importlib.import_module("yaml")
    except ImportError:
        from pip._internal import main
        main(["install", "pyyaml==5.1.1"])
    finally:
        yaml = importlib.import_module("yaml") 
    config = {}
    with open('tool.config') as project_config_file:
        project_config = project_config_file.read()
        config = yaml.load(project_config, Loader=yaml.BaseLoader)

    return config

    

def create_deployment(env):
    with open('tools/deploy.yaml') as app_file:
        app = app_file.read()

    app = app.replace('$ENV_NAME$', env['name'])
    
    with open('app.yaml', 'w') as app_file:
        app_file.write(app)

    return True
    

def create_config(env, project_config):
    to_replace = project_config['project'].get('env-vars', [])
    template_file = open(CONFIG_TEMPLATE, 'r')
    config = template_file.read()
    template_file.close()

    for r in to_replace:
        r1 = '$%s$' % (r)
        config = config.replace(r1, env[r.lower()])

    config_file = open(CONFIG_DEST, 'w')
    config_file.write(config)
    config_file.close()

    return True

def get_env_variables(project_config):
    env_vars = project_config['project'].get('env-vars', [])
    ref_slug = os.environ\
                .get('CI_COMMIT_REF_SLUG', '')\
                .replace('-','_')\
                .lower()
    env_sha = os.environ.get('CI_COMMIT_SHA','')[:8]
    env = project_config['project'].get('name')
    if ref_slug == 'master':
        env_name = env
    elif ref_slug == 'develop':
        env_name = env + '-staging'
    else:
        env_name = env + '-' + env_sha

    env = {
        'ref': ref_slug,
        'sha': env_sha,
        'name': env_name
    }

    for v in env_vars:
        if ref_slug == 'master':
            env[v.lower()] = os.environ.get(v.upper(), '')
            continue

        env[v.lower()] = os.environ.get(
            '%s_%s' % (env_name, v.upper()),
            ''
        )
    
    return env

def main():
    project_config = get_project_config()
    env = get_env_variables(project_config)
    create_config(env, project_config)
    create_deployment(env)

    return True

if __name__ == "__main__":
    main()