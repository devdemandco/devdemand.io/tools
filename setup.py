from setuptools import setup, find_packages
from .create_config import get_project_config

conf = get_project_config()
scripts = {
    'console_scripts': [
        '{}={}'.format(conf['cli']['alias'], "tools.manage:cli")
    ]
}

setup(
    name=conf['project']['name'],
    version=conf['project']['version'],
    url=conf['project'].get('url', ''),
    author=conf['project'].get('author', ''),
    author_email=conf['project'].get('author_email', ''),
    license=conf['project'].get('licence', 'Commercial'),
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python :: 3.6',
    ],
    keywords=conf['project'].get('keywords'),
    install_requires=conf['project']['dependencies'] + ['click', 'gunicorn'],
    tests_require=[],
    extras_require={
        'dev': [],
        'test': []
    },
    entry_points=scripts
)
