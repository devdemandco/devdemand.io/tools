#!/bin/bash

# The purpose of this file is to assist in setting up a
# local development environment.


# Check to see if the script has been forked or not (we need it to not be)
[[ $0 != "$BASH_SOURCE" ]] && sourced=1 || sourced=0

if [[ $sourced == 0 ]]; then
  echo -e "Please run this script using 'source'. e.g. \n"
  echo -e "\tsource ./tools/start-dev.sh\n"
  exit 1
fi

source_file="$(/usr/bin/env python3 ./tools/setup_env.py)"
source $source_file
